# Unix log file splitter

## Description

This script take the path of a unix log file as argument and split it into multiple smallest log file (at the same place that the source) using the process id (PID).

## Exmaple

    ./splitlog.rb /var/log/system.log

It will split the `/var/log/system.log` file into multiple files like `/var/log/123_splited_system.log`.

Have a look at the `example` folder.