#!/usr/bin/env ruby
#
# Split a big unix log file into multiple small log files using the process ID (PID)
#
# Author: zedtux (Guillaume HAIN)
# 2012
#

raise ArgumentError, "You must pass the path of the source log file" unless ARGV.size == 1

if File.exists?(ARGV.first)
  puts "Using #{ARGV.first} as source file"
else
  puts "Unable to access the file \"#{ARGV.first}\""
  exit
end

working_dir = File.dirname(ARGV.first)
source_filename = File.basename(ARGV.first)

File.open(ARGV.first, "r") do |file|
  files = {}
  file.readlines.each_with_index do |line, index|

    process_id = line.scan(/\[(\d+)\].*$/).flatten.first
    process_id = process_id.nil? ? "others" : process_id # use others.txt if no PID found

    # Store new file or reuse previous file
    files[process_id] ||= File.open(File.join(working_dir, "#{process_id}_splited_#{source_filename}"), "w")
    files[process_id].write(line)
  end
  
  # Close created files
  files.each{|id, file| file.close}

  puts "#{File.basename(ARGV.first)} have been splited into #{files.size} file#{"s" if files.size > 1} created." unless files.size.zero?
  puts "No file created." if files.size.zero?
end